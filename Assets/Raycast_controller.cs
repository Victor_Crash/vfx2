﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ludiq;
using Bolt;

//Haptic object = set the base/display obj;
//Haptic extension = set shape of the end-effector;

public class Raycast_controller : MonoBehaviour
{
    public Transform hapticObject = null;    // Предмет, с которым взаимодействуем. Необязателен если на 28 строке заменяем на forward
    public Transform handDataObject = null; // Искуственная "рука"
    //public HandDataController handDataController = null;// если используем лип
    public LayerMask hapticObjectMask = 0;  //Слой, по которому проверяем нужный ли предмет Настраивается в Layer Инспектора
    public float proximityRange = 1.0f;     //Дальность луча

    private Vector3 proximityObjectPos = Vector3.zero;
    private Vector3 proximityObjectDirection = Vector3.zero;

    private Vector3 hapticTargetPosition = Vector3.zero;
    private Vector3 hapticTargetNormal = Vector3.zero;

    private void FixedUpdate()
    {
            proximityObjectPos = handDataObject.position; 
             //proximityObjectPos = handDataController.GetPalmPosition(); // Если используем лип
             proximityObjectDirection = (hapticObject.position - proximityObjectPos).normalized;// Тут можно заменить на handDataObject.forward или right для проверки пересечения с объектом

        RaycastProximityPoint();
    }

    private void RaycastProximityPoint()
    {
        RaycastHit raycastHit;

        if (Physics.Raycast(proximityObjectPos, proximityObjectDirection, out raycastHit, proximityRange, hapticObjectMask))  
        {
            hapticTargetNormal = raycastHit.normal;
            hapticTargetPosition = raycastHit.point;
			Debug.Log("Hello: ");
			Variables.ActiveScene.Set("activate_first_vfx", "True");
        }
		else
		{
			Variables.ActiveScene.Set("activate_first_vfx", "False");
		}
    }

    public Vector3 GetTagertPos()
    {
        return hapticTargetPosition;
    }

    public Vector3 GetTargetNormal()
    {
        return hapticTargetNormal;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawLine(hapticTargetPosition, hapticTargetPosition + hapticTargetNormal);
        Gizmos.DrawSphere(hapticTargetPosition, 0.01f);
    }

}

