﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class vfxcontroller : MonoBehaviour
{
	
	[SerializeField]
	private VisualEffect visualEffect;
    // Start is called before the first frame update
	
	[SerializeField, Range(0,10)]
	private float velocity = 0;
	
	[SerializeField, Range(0,15)]
	private float lifetime = 0;
	
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		visualEffect.SetFloat("velocity", velocity);
		visualEffect.SetFloat("lifetime", lifetime);
		
        
    }
}
